package com.instil.webflix;

import com.google.common.collect.ImmutableList;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static com.google.common.base.Predicates.or;
import static springfox.documentation.builders.PathSelectors.ant;

@SpringBootApplication
@EnableSwagger2
public class WebflixApiApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(WebflixApiApplication.class);
	}
	
	public static void main(String[] args) {
		SpringApplication.run(WebflixApiApplication.class, args);
	}

	@Bean
    public Docket webflixApi() {
	    return new Docket(DocumentationType.SWAGGER_2)
                .groupName("webflix")
                .apiInfo(apiInfo())
                .securitySchemes(ImmutableList.of(jwt()))
                .select()
                    .paths(
                            or(
                                ant("/api/**"),
                                ant("/auth/**"),
                                ant("/register")
                    ))
                .build();
    }

	private ApiInfo apiInfo() {
	    return new ApiInfoBuilder()
                .title("Webflix API")
                .description("The API for the Webflix service.")
                .version("1.0")
                .build();
    }

    private ApiKey jwt() {
	    return new ApiKey("Authorization", "auth_token", "header");
    }

}
