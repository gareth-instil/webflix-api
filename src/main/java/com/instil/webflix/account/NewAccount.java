package com.instil.webflix.account;

import lombok.Data;

@Data
public class NewAccount {
    private String username;
    private String password;
    private String fullname;
}
