package com.instil.webflix.account;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Api(
    tags={"accounts"},
    description="Manage user accounts.",
    produces="application/json"
)
@RestController
public class AccountController {

    @Autowired
    private AccountService accountService;

    @ApiOperation(
        value="Register new user account.",
        httpMethod="POST"
    )
    @RequestMapping(value="/register", method=RequestMethod.POST)
    public ResponseEntity<Account> registerAccount(
            @ApiParam(name="newAccount", required=true)
            @RequestBody
            NewAccount newAccount) {
        Account createdAccount = accountService.registerAccount(newAccount);
        if (createdAccount == null) {
            return new ResponseEntity<Account>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<Account>(createdAccount, HttpStatus.CREATED);
    }

}
