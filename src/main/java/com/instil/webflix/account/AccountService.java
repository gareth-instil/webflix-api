package com.instil.webflix.account;

import com.instil.webflix.tools.Hashing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;

    public Account registerAccount(NewAccount newAccount) {
        Account account = accountFrom(newAccount);
        account.setSalt(Hashing.generateSalt());
        account.setPassword(Hashing.hash(account.getPassword(), account.getSalt()));
        return accountRepository.save(account);
    }

    private Account accountFrom(NewAccount newAccount) {
        return new AccountBuilder()
                .withUsername(newAccount.getUsername())
                .withPassword(newAccount.getPassword())
                .withFullname(newAccount.getFullname())
                .build();
    }

}
