package com.instil.webflix.account;

import com.instil.webflix.tools.Hashing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AccountVerifier {

    @Autowired
    private AccountRepository accountRepository;

    public boolean userIsVerified(String username, String password) {
        Account account = accountRepository.findByUsername(username);
        if (account == null) {
            return false;
        }
        return passwordIsVerified(account.getPassword(), account.getSalt(), password);
    }

    private boolean passwordIsVerified(String accountPassword, String accountSalt, String passwordToVerify) {
        String hashedPassword = Hashing.hash(passwordToVerify, accountSalt);
        return hashedPassword.equals(accountPassword);
    }

}
