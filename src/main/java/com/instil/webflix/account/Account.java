package com.instil.webflix.account;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name="accounts")
@Data
@NoArgsConstructor
public class Account {

    @Id
    @GeneratedValue
    private UUID id;

    private String username;

    private String password;

    private String salt;

    private String fullname;

    @ManyToMany(fetch=FetchType.EAGER)
    @JoinTable(name="account_roles",
               joinColumns={@JoinColumn(name="account_id", referencedColumnName="id")},
               inverseJoinColumns={@JoinColumn(name="role_id", referencedColumnName="id")})
    private List<Role> roles;
}