package com.instil.webflix.account;

public class AccountBuilder {

    private final Account account;

    public AccountBuilder() {
        this.account = new Account();
    }

    public AccountBuilder withUsername(String username) {
        this.account.setUsername(username);
        return this;
    }

    public AccountBuilder withPassword(String password) {
        this.account.setPassword(password);
        return this;
    }

    public AccountBuilder withFullname(String fullname) {
        this.account.setFullname(fullname);
        return this;
    }

    public Account build() {
        return account;
    }

}
