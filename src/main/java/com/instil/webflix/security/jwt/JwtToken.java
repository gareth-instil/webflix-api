package com.instil.webflix.security.jwt;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class JwtToken {
    public String token;
}
