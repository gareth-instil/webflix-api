package com.instil.webflix.security.jwt;

import com.instil.webflix.account.AccountVerifier;
import com.instil.webflix.security.LoginDetails;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(
    tags={"authentication"},
    description="Manage authentication tokens.",
    produces="application/json"
)
public class JwtController {

    @Autowired
    private JwtTokenUtil tokenFactory;

    @Autowired
    private AccountVerifier accountVerifier;

    @RequestMapping(value="${jwt.route.token}", method=RequestMethod.POST)
    @ApiOperation(
        value="Get a new token.",
        httpMethod="POST"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name="loginDetails", dataType="LoginDetails", required=true)
    })
    public ResponseEntity<JwtToken> getToken(@RequestBody LoginDetails loginDetails) {
        if (!accountVerifier.userIsVerified(loginDetails.getUsername(), loginDetails.getPassword())) {
            return new ResponseEntity<JwtToken>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<JwtToken>(tokenFactory.generateToken(loginDetails), HttpStatus.OK);
    }

}
