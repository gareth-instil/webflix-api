package com.instil.webflix.security;

import lombok.Data;

@Data
public class LoginDetails {
    private String username;
    private String password;
}
