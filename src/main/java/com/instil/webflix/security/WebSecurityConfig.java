package com.instil.webflix.security;

import com.instil.webflix.security.jwt.JwtAuthenticationEntryPoint;
import com.instil.webflix.security.jwt.JwtFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtAuthenticationEntryPoint unauthorizedHandler;

    @Value("${jwt.route.token}")
    private String tokenPath;

    @Value("${jwt.route.refresh}")
    private String refreshPath;

    @Autowired
    private JwtFilter jwtFilter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        String registerPath = "/register";
        http
            .csrf().disable()
            .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
            .authorizeRequests()
            .antMatchers(HttpMethod.GET, swaggerPaths()).permitAll()
            .antMatchers(HttpMethod.POST, registerPath, tokenPath, refreshPath).permitAll()
            .anyRequest().authenticated();
        http.addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class);
        http.headers().cacheControl();
    }

    private String[] swaggerPaths() {
        return new String[] {
            "/swagger-ui.html",
            "/webjars/springfox-swagger-ui/**",
            "/v2/api-docs/**",
            "/swagger-resources/**"
        };
    }
}
