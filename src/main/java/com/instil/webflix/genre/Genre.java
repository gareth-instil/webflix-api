package com.instil.webflix.genre;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name="genres")
@Data
@NoArgsConstructor
public class Genre {
    @Id private UUID id;
    private String value;
}
