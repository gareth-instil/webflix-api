package com.instil.webflix.movie;

import com.instil.webflix.classification.Classification;
import com.instil.webflix.genre.Genre;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name="movies")
@Data
@NoArgsConstructor
public class Movie {

    @Id
    private UUID id;

    @ManyToOne(cascade=CascadeType.PERSIST)
    @JoinColumn(name="genre")
    private Genre genre;

    @ManyToOne(cascade=CascadeType.PERSIST)
    @JoinColumn(name="classification")
    private Classification classification;

    private String title;

    private String year;

}
