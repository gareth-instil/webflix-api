package com.instil.webflix.movie;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

import static org.springframework.data.jpa.domain.Specifications.where;
import static com.instil.webflix.movie.MovieSpecifications.*;

@Service
public class MovieService {

    @Autowired
    private MovieRepository movieRepository;

    public List<Movie> findAllMoviesMatching(String searchField, String searchTerm) {
        if (searchField == null || searchTerm == null) {
            return findAllMovies();
        }
        switch(searchField) {
            case "between":
                String[] years = searchTerm.split("-");
                String startYear = years[0];
                String endYear = years[1];
                return findMoviesMadeBetween(startYear, endYear);
            case "year":
                return findMoviesMadeIn(searchTerm);
            case "title":
                return findMoviesCalledSomethingLike(searchTerm);
            default:
                return findAllMovies();
        }
    }

    private List<Movie> findMoviesCalledSomethingLike(String searchTerm) {
        return asList(movieRepository.findAll(where(titleLike(searchTerm))));
    }

    private List<Movie> findAllMovies() {
        return asList(movieRepository.findAll());
    }

    private List<Movie> findMoviesMadeBetween(String startYear, String endYear) {
        return asList(movieRepository.findAll(where(yearBetween(startYear, endYear))));
    }

    private List<Movie> findMoviesMadeIn(String year) {
        return asList(movieRepository.findAll(where(yearIs(year))));
    }

    private List<Movie> asList(Iterable<Movie> movies) {
        if (movies == null) { // Urgh!
            return null;
        }
        return Lists.newArrayList(movies);
    }

    public Movie findById(UUID id) {
        return movieRepository.findOne(id);
    }
}
