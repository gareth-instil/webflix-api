package com.instil.webflix.movie;

import org.springframework.data.jpa.domain.Specification;

import java.util.function.Predicate;

public class MovieSpecifications {

    public static Specification<Movie> titleLike(String title) {
        return (movie, query, builder) -> builder.like(movie.get("title"), title);
    }

    public static Specification<Movie> yearIs(String year) {
        return (movie, query, builder) -> builder.equal(movie.get("year"), year);
    }

    public static Specification<Movie> yearBetween(String startYear, String endYear) {
        return (movie, query, builder) -> builder.between(movie.get("year"), startYear, endYear);
    }
}
