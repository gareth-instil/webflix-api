package com.instil.webflix.movie;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/movies")
@Api(
    tags={"movies"},
    description="Manage movie list.",
    produces="application/json",
    basePath="/api/movies"
)
public class MovieController {

    @Autowired
    private MovieService movieService;

    @RequestMapping()
    @ApiOperation(
        value="Get a list of movies.",
        httpMethod="GET"
    )
    public List<Movie> getMovies(
            @ApiParam(required=false, value="Search term")
            @RequestParam(value="q",required=false)
                    String searchTerm,
            @ApiParam(required=false, value="Search field", allowableValues="title,year,between", defaultValue="title")
            @RequestParam(value="qf",required=false)
                    String searchField
        ) {
        return movieService.findAllMoviesMatching(searchField, searchTerm);
    }

    @RequestMapping(value="/{id}")
    @ApiOperation(
        value="Get a movie by its ID",
        httpMethod="GET"
    )
    public Movie getMovieById(@PathVariable(name="id")UUID id) {
        return movieService.findById(id);
    }

}
