package com.instil.webflix.movie;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface MovieRepository extends CrudRepository<Movie, UUID>,
        JpaSpecificationExecutor<Movie> {
}
