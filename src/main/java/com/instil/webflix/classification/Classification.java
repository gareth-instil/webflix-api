package com.instil.webflix.classification;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name="classifications")
@Data
@NoArgsConstructor
public class Classification {
    @Id private UUID id;
    private String value;
}
