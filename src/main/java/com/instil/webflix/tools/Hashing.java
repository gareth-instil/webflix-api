package com.instil.webflix.tools;

import com.google.common.base.Charsets;
import com.google.common.hash.Hasher;
import com.google.common.io.BaseEncoding;
import lombok.experimental.UtilityClass;

import java.security.SecureRandom;
import java.util.Random;

@UtilityClass
public class Hashing {

    public String generateSalt() {
        Random random = new SecureRandom();
        byte[] saltBytes = new byte[32];
        random.nextBytes(saltBytes);
        return BaseEncoding.base64().omitPadding().encode(saltBytes);
    }

    public static String hash(String... strings) {
        Hasher hasher = com.google.common.hash.Hashing.sha512().newHasher();
        for (String s : strings) {
            hasher.putString(s, Charsets.UTF_8);
        }
        return hasher.hash().toString();
    }
}
