CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE classifications (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    value VARCHAR(8) NOT NULL
);

CREATE TABLE genres (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    value VARCHAR(64) NOT NULL
);

CREATE TABLE movies (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    title VARCHAR(128) NOT NULL,
    year CHAR(4) NOT NULL,
    classification UUID REFERENCES classifications(id),
    genre UUID REFERENCES genres(id)
);

CREATE TABLE roles (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    role VARCHAR(32) NOT NULL
);

CREATE TABLE accounts (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    username VARCHAR(64) NOT NULL,
    password VARCHAR(128) NOT NULL,
    salt VARCHAR(64) NOT NULL,
    fullname VARCHAR(128)
);

CREATE TABLE account_roles (
    account_id UUID REFERENCES accounts(id),
    role_id UUID REFERENCES roles(id)
);