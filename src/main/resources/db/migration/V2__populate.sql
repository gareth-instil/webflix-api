DO $$

DECLARE
    horror UUID;
    adult UUID;

BEGIN

INSERT INTO genres(value) VALUES ('Horror');
INSERT INTO genres(value) VALUES ('Comedy');
INSERT INTO genres(value) VALUES ('Family');
INSERT INTO genres(value) VALUES ('Science Fiction');

INSERT INTO classifications(value) VALUES ('U');
INSERT INTO classifications(value) VALUES ('PG');
INSERT INTO classifications(value) VALUES ('PG-13');
INSERT INTO classifications(value) VALUES ('12');
INSERT INTO classifications(value) VALUES ('12A');
INSERT INTO classifications(value) VALUES ('15');
INSERT INTO classifications(value) VALUES ('18');

SELECT id INTO horror FROM genres WHERE value='Horror';
SELECT id INTO adult FROM classifications WHERE value='18';

INSERT INTO movies(title, year, genre, classification)
       VALUES ('The Terminator', '1984', horror, adult);
INSERT INTO movies(title, year, genre, classification)
       VALUES ('Commando', '1985', horror, adult);
INSERT INTO movies(title, year, genre, classification)
       VALUES ('Predator', '1987', horror, adult);
INSERT INTO movies(title, year, genre, classification)
       VALUES ('Total Recall', '1990', horror, adult);
INSERT INTO movies(title, year, genre, classification)
       VALUES ('True Lies', '1994', horror, adult);
INSERT INTO movies(title, year, genre, classification)
       VALUES ('Eraser', '1996', horror, adult);

INSERT INTO ROLES(role) VALUES ('ROLE_USER');
INSERT INTO ROLES(role) VALUES ('ROLE_ADMIN');

END$$