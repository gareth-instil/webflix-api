package com.instil.webflix.movie;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class MovieServiceTest {

    @Mock
    MovieRepository movieRepository;

    @InjectMocks
    MovieService target;

    @Test
    public void shouldReturnAllMoviesIfNoSearchCriteriaIsSupplied() {
        target.findAllMoviesMatching(null, null);
        verify(movieRepository).findAll();
    }

}